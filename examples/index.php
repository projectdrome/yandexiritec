<?php
require '../autoload.php';

use YandexIritec as C;
use YandexIritec\Validate\Exception as ValEx;

$db = new C\DB\MySQL('mysql://yandexiritec:N6m_dJ2ajd@localhost:/yandexiritec');
$xml = new C\Xml(file_get_contents('/tmp/test.xml'));

/**
 * $xml->validate($xsd_string);
 * Validates against xsd
 */


foreach ($xml->find('/ns1:realty-feed/ns1:offer') as $node) {
    try {
        //1-level data validation
        $node->validate([
            'expire-date' => ['rq' => true, 'filter' => function ($data) {return (bool)strtotime($data);}],
            'rooms' => ['rq' => false, 'rgx' => '/^[0-9]{1,2}$/'],
        ]);
        
        //child nodes validation
        $node->child('sales-agent')->validate([
            'phone' => [
                'rq' => true,
                'filter' => function ($data) {
                    $data = preg_replace('/[^0-9]/', '', $data);
                    $len = strlen($data);
                    
                    //11-15 len range - international msisdn standart
                    return !empty($data) && $len >= 11 && $len <= 15;
                }
            ]
        ]);
        
        
        $db->put('realty_objects', $node);
    } 
    catch (ValEx\ParamMissing $ex) {
        //exception handlers
    }
    catch (ValEx\BadParamValue $ex) {
        //exception handlers
    }
}
?>

