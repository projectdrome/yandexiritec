<?php
namespace YandexIritec;
/**
 * Description of Container
 *
 * @author Pavel Napolov <p@projectdrome.com>
 */
class Container {
    /**
     * @var array
     */
    protected $array;
    
    /**
     * @var \Closure 
     */
    protected $callback;
    
    /**
     * @param array $input
     * @param \Closure $callback global callback handler
     */
    public function __construct(array $input, $callback = false) {
        $this->array = $input;
        if ($callback instanceof \Closure)
            $this->callback = $callback;
    }
    
    /**
     * @return \YandexIritec\Container
     */
    public function child($key = false) {
        if (isset($this->array[$key])) 
            return new self((is_array($this->array[$key]) ? $this->array[$key] : [$this->array[$key]]), $this->callback);
        else 
            return null;    
    }
    
    /**
     * Dumps contents
     * 
     * @return array
     */
    public function get() {
        return $this->array;
    }
    
    /**
     * Runs global callback
     * @return mixed 
     */
    public function callback() {
        return $this->callback($this->array);
    }
    
    /**
     * @param array $mask
     * @return boolean
     * @throws \YandexIritec\Validate\Exception\ParamMissing
     * @throws \YandexIritec\Validate\Exception\BadParamValue
     */
    public function validate($mask) {
        foreach ($mask as $k => $v) {
            if ($v['rq'] === true && !isset($this->array[$k])) 
                throw new \YandexIritec\Validate\Exception\ParamMissing($k);
            
            if (isset($v['filter']) && ($v['filter'] instanceof \Closure) && !$v['filter']($this->array[$k]))
                throw new \YandexIritec\Validate\Exception\BadParamValue(implode(' | ', [$k, $this->array[$k]]));
            
            if (isset($v['rgx']) && $v['rgx'] && !preg_match($v['rgx'], $this->array[$k]))
                throw new \YandexIritec\Validate\Exception\BadParamValue(implode(' | ', [$k, $this->array[$k], $v['rgx']]));
        }
        
        return true;
    }
}

