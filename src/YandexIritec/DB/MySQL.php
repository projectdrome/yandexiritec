<?php
namespace YandexIritec\DB;
/**
 * Database driver wrapper
 *
 * @author Pavel Napolov <p@projectdrome.com>
 */
class MySQL {
    /**
     * @var array 
     */
    protected static $connection;
    /**
     * @var \mysqli 
     */
    protected static $driver;
    
    /**
     * @param string $connect_string RFC2396 url
     * @throws Exception\ConnectFailed
     */
    public function __construct($connect_string) {
        if (!self::$driver) {
            self::$connection = parse_url($connect_string);
            self::$connection['database'] = str_replace('/', '', self::$connection['path']);
            self::$driver = new \mysqli(
                self::$connection['host'], 
                self::$connection['user'], 
                self::$connection['pass'], 
                self::$connection['database']);
            
            if (\mysqli_connect_errno()) 
                throw new Exception\ConnectFailed($connect_string.' '.\mysqli_connect_error());
        }
    }
    
    /**
     * @param string $table
     * @param \YandexIritec\Container $element
     * @return int
     * @throws Exception\QueryFailed
     */
    public function put($table, \YandexIritec\Container $element) {
        $cols = [];
        $vals = [];
        
        foreach ($element->get() as $k => $v) {
            $cols[] = '`'.self::$driver->real_escape_string($k).'`';
            $vals[] = '"'.self::$driver->real_escape_string(
                is_object($v) || is_array($v) ? serialize($v) : $v).'"';       
        }
        
        $query = 'INSERT INTO `'.self::$driver->real_escape_string($table).'`('.implode(',', $cols).') VALUES('.implode(',', $vals).')';
        /*$result = self::$driver->real_query($query);
        
        if (self::$mysqli->errno)
            throw new Exception\QueryFailed('Error # '.self::$mysqli->errno.': '.self::$mysqli->error."\n".$query);
        
        return self::$mysqli->insert_id;*/
        
        return $query;
    }
}
