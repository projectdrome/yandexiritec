<?php
namespace YandexIritec;
/**
 * Basic XML parser
 *
 * @author Pavel Napolov <p@projectdrome.com>
 */
class Xml {
    /**
     *
     * @var \SimpleXMLElement
     */
    protected $smx;
    
    /**
     * @param string $xml_string
     */
    public function __construct($xml_string) {
        $this->smx = \simplexml_load_string($xml_string, 'SimpleXMLElement', LIBXML_NOCDATA);
        $c = 1;
        foreach ($this->smx->getNamespaces() as $ns) {
            $this->smx->registerXPathNamespace('ns'.$c, $ns);
            $c++;
        }
    }
    
    /**
     * 
     * @param string $xpath_string
     * @return \YandexIritec\Container
     */
    public function find($xpath_string) {
        $result = [];
        
        foreach($this->smx->xpath($xpath_string) as $node)
            $result[] = new Container(json_decode(json_encode($node), true));
        
        return sizeof($result) === 0 ? $result[0] : $result;
    }
    
    /**
     * Validates xml against xsd
     * 
     * @param string $xsd_string
     * @return bool
     */
    public function validate($xsd_string = false)
    {
        $dom = new \DOMDocument();
        $dom->loadXML($this->smx->asXML());
        
        return $xsd_string ? 
            $dom->schemaValidateSource($xsd_string) :
            $dom->schemaValidate($this->smx->getNamespaces()[0]);
    }
}
